namespace _2017Blog.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Yorum")]
    public partial class Yorum
    {
        [Key]
        public int Yorum_ID { get; set; }

        [Required]
        [StringLength(500)]
        public string Icerik { get; set; }

        public int UyeID { get; set; }

        public int MakaleID { get; set; }

        public DateTime Tarih { get; set; }

        public virtual Kullanici Kullanici { get; set; }

        public virtual Makaleler Makaleler { get; set; }

        public virtual Uye Uye { get; set; }
    }
}
