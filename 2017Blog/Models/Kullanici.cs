namespace _2017Blog.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Kullanici")]
    public partial class Kullanici
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Kullanici()
        {
            Yorums = new HashSet<Yorum>();
        }

        [Key]
        public int Kullanici_ID { get; set; }

        [Required]
        [StringLength(20)]
        public string Kullanici_Nick { get; set; }

        [Required]
        [StringLength(20)]
        public string Kullanici_Sifre { get; set; }

        [StringLength(50)]
        public string Kullanici_Isım { get; set; }

        [StringLength(50)]
        public string Kullanici_Soyad { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Yorum> Yorums { get; set; }
    }
}
